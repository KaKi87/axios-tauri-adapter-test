# axios-tauri-adapter-test

Unit tests for [axios-tauri-adapter](https://git.kaki87.net/KaKi87/axios-tauri-adapter), powered by [QUnit](https://qunitjs.com/).

![](https://i.goopics.net/27gwec.png)

## Detected Tauri issues

| Issue                                                                                                                  | Fix                                                                                               |
|------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| [http.fetch always expects a response body](https://github.com/tauri-apps/tauri/issues/2831)                           | [1.0.0-rc.0](https://github.com/tauri-apps/tauri/commit/50c63900c7313064037e2ceb798a6432fcd1bcda) |
| [http.ClientOptions.maxRedirections not working](https://github.com/tauri-apps/tauri/issues/4795)                      | [1.0.7](https://github.com/tauri-apps/tauri/commit/d576e8ae72b025ca41f96ddf7a885b84f950a4b1)      |
| [HTTP 100 response inconsistent between default and "reqwest-client"](https://github.com/tauri-apps/tauri/issues/4796) | N/A                                                                                               |