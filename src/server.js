import fs from 'node:fs/promises';

import createFastify from 'fastify';
import basicAuth from 'basic-auth';
import TOML from 'toml';

import {
    METHODS,
    REDIRECT_RESPONSE_CODES
} from './constants.js';

const fastify = createFastify();

fastify.route({
    method: METHODS,
    url: '/code/:code',
    handler: ({
        method,
        params: path,
        query: params,
        headers,
        body: data
    }, reply) => {
        const
            { code } = path,
            {
                'authorization': authenticationHeader
            } = headers,
            {
                'name': username,
                'pass': password
            } = authenticationHeader ? basicAuth.parse(authenticationHeader) : {};
        reply.code(parseInt(code));
        if(REDIRECT_RESPONSE_CODES.includes(parseInt(code)))
            reply.header('location', '/code/200');
        reply.send({
            method,
            headers,
            params,
            data,
            username,
            password
        });
    }
});

fastify.get(
    '/Cargo.toml',
    async (
        request,
        reply
    ) => {
        reply.send(TOML.parse(await fs.readFile('./src-tauri/Cargo.toml', 'utf8')));
    }
);

fastify
    .listen({ port: 5678 })
    .then(() => console.log('Server running'))
    .catch(console.error);