import axios from 'axios';
import axiosTauriAdapter from 'axios-tauri-adapter';

import { QUnit } from 'qunit';

import {
    METHODS,
    RESPONSE_TYPES,
    RESPONSE_CODES,
    REDIRECT_RESPONSE_CODES
} from './constants.js';

QUnit.config.autostart = false;

const
    baseURL = 'http://localhost:5678',
    client = axios.create({
        adapter: axiosTauriAdapter,
        baseURL
    }),
    generateRandomString = () => [...crypto.getRandomValues(new Uint8Array(10))].map(_ => _.toString(16)).join('');

(async () => {
    const cargoToml = (await client.get('/Cargo.toml')).data;
    QUnit.start();

    METHODS.forEach(sentMethod => {
        let res, err;
        const
            sentParams = { [generateRandomString()]: generateRandomString() },
            sentHeaders = { [generateRandomString()]: generateRandomString() },
            sentData = { [generateRandomString()]: generateRandomString() };
        QUnit.module(`config.method: "${sentMethod}"`, {
            before: assert => {
                const done = assert.async();
                (async () => {
                    try {
                        res = await client({
                            url: '/code/200',
                            method: sentMethod,
                            params: sentParams,
                            headers: sentHeaders,
                            ...['POST', 'PUT', 'PATCH'].includes(sentMethod) ? {
                                data: sentData
                            } : {}
                        });
                    }
                    catch(_err){ err = _err; }
                    done();
                })();
            }
        });
        QUnit.test('Request succeeds', assert => {
            assert.equal(typeof res, 'object', 'Request response is an object');
            assert.notOk(err, 'Request error does not exist');
        });
        QUnit.test('Request configuration matches', assert => {
            if(sentMethod === 'HEAD') assert.deepEqual(res.data, {}, 'Request response data is empty');
            else {
                const {
                    data: {
                        method: receivedMethod,
                        params: receivedParams,
                        headers: receivedHeaders
                    }
                } = res;
                assert.equal(receivedMethod, sentMethod, 'Request method matches');
                assert.deepEqual(receivedParams, sentParams, 'Request params match');
                assert.deepEqual(receivedHeaders, { ...receivedHeaders, ...sentHeaders }, 'Request headers match');
            }
        });
    });

    {
        let res, err;
        const
            sentUsername = generateRandomString(),
            sentPassword = generateRandomString();
        QUnit.module('config.auth', {
            before: assert => {
                const done = assert.async();
                (async () => {
                    try {
                        res = await client({
                            url: '/code/200',
                            method: 'GET',
                            auth: {
                                username: sentUsername,
                                password: sentPassword
                            }
                        });
                    }
                    catch(_err){ err = _err; }
                    done();
                })();
            }
        });
        QUnit.test('Request succeeds', assert => {
            assert.equal(typeof res, 'object', 'Request response is an object');
            assert.notOk(err, 'Request error does not exist');
        });
        QUnit.test('Request configuration matches', assert => {
            const {
                data: {
                    username: receivedUsername,
                    password: receivedPassword
                }
            } = res;
            assert.equal(receivedUsername, sentUsername, 'Request BasicAuth username matches');
            assert.equal(receivedPassword, sentPassword, 'Request BasicAuth password matches');
        });
    }

    RESPONSE_TYPES.forEach(responseType => {
        let res, err;
        QUnit.module(`config.responseType: "${responseType}"`, {
            before: assert => {
                const done = assert.async();
                (async () => {
                    try {
                        res = await client({
                            url: '/code/200',
                            responseType,
                            transformResponse: responseType === 'text' ? data => data : undefined
                        });
                    }
                    catch(_err){ err = _err; }
                    done();
                })();
            }
        });
        QUnit.test('Request succeeds', assert => {
            assert.equal(typeof res, 'object', 'Request response is an object');
            assert.notOk(err, 'Request error does not exist');
        });
        QUnit.test('Request configuration matches', assert => {
            assert.equal(
                res.data.constructor.name,
                {
                    'arraybuffer': 'ArrayBuffer',
                    'json': 'Object',
                    'text': 'String',
                    'blob': 'Blob'
                }[responseType]
            );
        });
    });

    RESPONSE_CODES.forEach(responseCode => {
        const expectedStatus = REDIRECT_RESPONSE_CODES.includes(responseCode) ? 200 : responseCode;
        let res, err;
        QUnit.module(`res.status: "${responseCode}"`, {
            before: assert => {
                const done = assert.async();
                (async () => {
                    try {
                        res = await client({
                            url: `/code/${responseCode}`,
                            validateStatus: status => status === expectedStatus
                        });
                    }
                    catch(_err){ err = _err; }
                    done();
                })();
            }
        });
        QUnit.test('Request succeeds', assert => {
            assert.equal(typeof res, 'object', 'Request response is an object');
            assert.notOk(err, 'Request error does not exist');
        });
        QUnit.test('Response status code matches', assert => {
            assert.equal(res.status, expectedStatus);
        });
        QUnit.test('Response status text exists', assert => {
            assert.equal(typeof res.statusText, 'string');
        });
    });

    REDIRECT_RESPONSE_CODES.forEach(responseCode => {
        let res, err;
        QUnit.module(`conf.maxRedirects: "0", res.status: "${responseCode}"`, {
            before: assert => {
                const done = assert.async();
                (async () => {
                    try {
                        res = await client({
                            url: `/code/${responseCode}`,
                            validateStatus: status => status === responseCode,
                            maxRedirects: 0
                        });
                    }
                    catch(_err){ err = _err; }
                    done();
                })();
            }
        });
        QUnit.test('Request succeeds', assert => {
            assert.equal(typeof res, 'object', 'Request response is an object');
            assert.notOk(err, 'Request error does not exist');
        });
        QUnit.test('Response status code matches', assert => {
            assert.equal(res.status, responseCode);
        });
    });
})().catch(console.error);