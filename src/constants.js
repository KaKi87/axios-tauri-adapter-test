import { StatusCodes } from 'http-status-codes';

export const METHODS = [
    // https://github.com/axios/axios/blob/v1.x/index.d.ts#L106
    // https://github.com/tauri-apps/tauri/blob/v1.0.3/tooling/api/src/http.ts#L188
    'GET',
    'DELETE',
    'HEAD',
    'OPTIONS',
    'POST',
    'PUT',
    'PATCH'
];

export const RESPONSE_TYPES = [
    // https://github.com/axios/axios/blob/v1.x/index.d.ts#L118
    'arraybuffer',
    'blob',
    'json',
    'text'
];

export const RESPONSE_CODES = Object.values(StatusCodes).filter(_ => typeof _ === 'number' && ![100, 102].includes(_));

export const REDIRECT_RESPONSE_CODES = Object.entries(StatusCodes).filter(([key]) => key.startsWith('MOVED_') || key.startsWith('SEE_') || key.endsWith('_REDIRECT')).map(([, _]) => _);